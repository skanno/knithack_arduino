// glitched knitting machine
// 2013 3 26
// tomofumi


#include <LiquidCrystal.h>

//INPUT SYSTEM
const int enc1 = 27;  //カウント用エンコーダ
const int enc2 = 26;  //回転方向検知用エンコーダ
const int enc3 = 25;  //フェーズ更新用エンコーダ
const int bar = 24;    //段数計スイッチ
const int LEnd = 23;   //左エンドスイッチ
const int REnd = 22;   //右エンドスイッチ

//OUTPUT SYSTEM
const int led1 = 6;    //インジケータLED
const int led2 = 7;    //インジケータLED
const int led3 = 13;    //キャリッジ移動インジケータ

const int needle1 = 31;  //ニードル制御用
const int needle2 = 32;
const int needle3 = 33;
const int needle4 = 34;
const int needle5 = 35;
const int needle6 = 36;
const int needle7 = 37;
const int needle8 = 38;
const int needle9 = 39;
const int needle10 = 40;
const int needle11 = 41;
const int needle12 = 42;
const int needle13 = 43;
const int needle14 = 44;
const int needle15 = 45;
const int needle16 = 46;

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
// LCD RS to 12
// LCD Enable to 11
// LCD D4 to 5
// LCD D5 to 4
// LCD D6 to 3
// LCD D7 to 2
// LCD R/W to GND



int pos = 0;  //キャリッジの現在位置
int encState1 = 0;  //カウント用エンコーダの入力値
int encState2 = 0;  //回転方向検知用エンコーダの入力値
int lastState = 0;  //カウント用エンコーダの前回の値
int phaseState = 0; //フェーズ検知用エンコーダの入力値
int lastPhaseState = 0; //フェーズ検知用エンコーダの前回の値
int phase = 0;      //現在のフェーズ
int zero = 0;       //左エンドスイッチの入力値
int lastZero = 0;   //左エンドスイッチの前回の値
int right = 0;      //右エンドスイッチの入力値
int lastRight = 0;  //右エンドスイッチの前回の値
int barSwitch = 0;  //段数スイッチの入力値
int lastBarSwitch = 0;  //段数スイッチの前回の値
int barCounter = 0;    //現在の段数
int carDirection = 0;  //キャリッジの進行方向　0:不明　1:右方向　2:左方向


//for Serial data receiving
int dataSize = 1024;
char pixelBin[1024];



void setup(){
	pinMode(enc1, INPUT);
	pinMode(enc2, INPUT);
	pinMode(enc3, INPUT);
	pinMode(bar, INPUT);
	pinMode(LEnd, INPUT);
	pinMode(REnd, INPUT);

	pinMode(led1, OUTPUT);
	pinMode(led2, OUTPUT);
	pinMode(led3, OUTPUT);
	pinMode(needle1, OUTPUT);
	pinMode(needle2, OUTPUT);
	pinMode(needle3, OUTPUT);
	pinMode(needle4, OUTPUT);
	pinMode(needle5, OUTPUT);
	pinMode(needle6, OUTPUT);
	pinMode(needle7, OUTPUT);
	pinMode(needle8, OUTPUT);
	pinMode(needle9, OUTPUT);
	pinMode(needle10, OUTPUT);
	pinMode(needle11, OUTPUT);
	pinMode(needle12, OUTPUT);
	pinMode(needle13, OUTPUT);
	pinMode(needle14, OUTPUT);
	pinMode(needle15, OUTPUT);
	pinMode(needle16, OUTPUT);

	Serial.begin(57600);

	lcd.begin(16, 2);
}


void loop(){
	if(Serial.available() > 0){
		if(Serial.readBytesUntil('\n', pixelBin, dataSize)){
			for(int i=0; i < dataSize; i++){
				Serial.write(pixelBin[i]);
			}
			Serial.write('\n');
			memset(pixelBin, 0, sizeof(pixelBin));
			// Serial.end();
		}
	}


	encState1 = digitalRead(enc1);
	encState2 = digitalRead(enc2);
	zero = digitalRead(LEnd);
	right = digitalRead(REnd);
	barSwitch = digitalRead(bar);

	if(encState1 == HIGH){
		digitalWrite(led3, HIGH);
	}
	else{
		digitalWrite(led3,LOW);
	}


  //回転検知用エンコーダが反応したとき
	if(encState1 != lastState){
		if(encState1 == HIGH){
			carDir();
			// Serial.println(carDirection);

		}
	}


  //フェーズ検知用エンコーダが反応した時
	if(phaseState != lastPhaseState){
		if(phaseState == HIGH){
			phase = phase + 1;
			out();
		}
	}


  //左側エンドスイッチが反応した時
	if(zero != lastZero){
		if(zero == LOW){
			pos = 0;
		} 
	}

  //右側エンドスイッチが反応した時
	if(right != lastRight){
		if(right == LOW){
			pos = 200;
		} 
	}


  //段数計スイッチが反応した時
	if(barSwitch != lastBarSwitch){
		if(barSwitch == HIGH){
			barCounter = barCounter + 1;
		}
	}



  //各センサの前回値を更新

	lastBarSwitch = barSwitch;
	lastZero = zero;
	lastRight = right;
	lastState = encState1; 

  //lcd.print(pos);

}



// キャリッジの移動時に移動方向の認識と現在位置の更新
void carDir(){
  // 右方向へ進んでいる 
	if(encState2 == LOW){
		carDirection = 1;
		pos = pos + 1;
		// Serial.println(pos);
		lcd.clear();
		lcd.print(pos);
		lcd.print(' ');
		lcd.write(pixelBin[pos]+49);
	}
  //　左方向へ進んでいる
	else {
		carDirection = 2;
		pos = pos - 1;
		// Serial.println(pos);
		lcd.clear();
		lcd.print(pos);
		lcd.print(' ');
		lcd.write(pixelBin[pos]+49);
	}
}


//ニードルへ出力
void out(){
	digitalWrite(needle1, LOW);
}






//so's code below
/*
void loop() {
	// if(Serial.readBytesUntil('\n', pixelBin, dataSize)){
	// 	for(int i=0; i < dataSize; i++){
	// 		if(i == (dataSize-1)){
	// 			Serial.println(pixelBin[i]);
	// 		}else if(i < 3){
	// 			digitalWrite(i+9, pixelBin[i]);
	// 			Serial.print(pixelBin[i]);
	// 			Serial.print(",");				
	// 		}else{
	// 			Serial.print(pixelBin[i]);
	// 			Serial.print(",");
	// 		}
	// 	}
	// 	// Serial.write('\n');
	// 	memset(pixelBin, 0, sizeof(pixelBin));
	// }

	//this paragraph is for hardwareTest on 2013.3.26.
	// if(serial.available() )
		if(Serial.readBytesUntil('\n', pixelBin, dataSize)){
			for(int i=0; i < dataSize; i++){
				Serial.write(pixelBin[i]);
			}
			Serial.write('\n');
			memset(pixelBin, 0, sizeof(pixelBin));
		}
	// }
	

	// if(Serial.available() > 0){
	// 	int packet = Serial.parseInt();
	// 	pixelRead(packet);
	// }
}

// void pixelRead(int pac){
// 	if(Serial.readBytesUntil('\n', pixelBin[pac-2], dataSize)){
// 		for(int i=0; i < dataSize; i++){
// 			if(i < 3){
// 				digitalWrite(i+9, pixelBin[pac-2][i]);
// 				Serial.write(pixelBin[pac-2][i]);
// 			}else{
// 				Serial.write(pixelBin[pac-2][i]);
// 			}
// 		}
// 		Serial.write('\n');
// 		memset(pixelBin[pac-2], 0, sizeof(pixelBin[pac-2]));
// 	}
// }

*/